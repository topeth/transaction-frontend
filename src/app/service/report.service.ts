import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Report } from '../reports/report/report.component';
import { API_URL } from '../app.constants';

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private http: HttpClient) { }

  retriveReport() {
    return this.http.get<Report[]>(`${API_URL}/report`)
  }

  retriveSuccessReport() {
    return this.http.get<Report[]>(`${API_URL}/successreport`)
  }
  
  retriveFailureReport() {
    return this.http.get<Report[]>(`${API_URL}/failurereport`)
  }
}
