import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReportComponent } from './reports/report/report.component';
import { SuccessReportComponent } from './reports/success-report/success-report.component';
import { FailureReportComponent } from './reports/failure-report/failure-report.component';

const routes: Routes = [
  {path:'report', component: ReportComponent},
  {path:'success-report', component: SuccessReportComponent},
  {path:'failure-report', component: FailureReportComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
