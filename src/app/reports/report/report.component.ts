import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportService } from 'src/app/service/report.service';

export class Report {
  constructor(
    public id: number,
    public reference: string,
    public accountNumber: string,
    public description: string,
    public startBalance: string,
    public mutation: string,
    public endBalance: string,
    public status: string,
    public remarks: string,
    public source: string
  ) { }
}

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  reports: Report[] | any

  constructor(
    private reportService: ReportService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.refreshReport()
  }

  refreshReport(){
    this.reportService.retriveReport().subscribe(
      (response: any)  => {
        console.log(response)
        this.reports = response
      }
    )
  }
}
