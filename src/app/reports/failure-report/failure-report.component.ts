import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportService } from 'src/app/service/report.service';
import { Report } from '../report/report.component';

@Component({
  selector: 'app-failure-report',
  templateUrl: './failure-report.component.html',
  styleUrls: ['./failure-report.component.css']
})
export class FailureReportComponent implements OnInit {

  reports: Report[] | any

  constructor(
    private reportService: ReportService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.refreshReport()
  }

  refreshReport(){
    this.reportService.retriveFailureReport().subscribe(
      (response: any)  => {
        console.log(response)
        this.reports = response
      }
    )
  }

}
