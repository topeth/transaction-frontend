import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FailureReportComponent } from './failure-report.component';

describe('FailureReportComponent', () => {
  let component: FailureReportComponent;
  let fixture: ComponentFixture<FailureReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FailureReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FailureReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
