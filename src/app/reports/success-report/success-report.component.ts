import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportService } from 'src/app/service/report.service';
import { Report } from '../report/report.component';

@Component({
  selector: 'app-success-report',
  templateUrl: './success-report.component.html',
  styleUrls: ['./success-report.component.css']
})
export class SuccessReportComponent implements OnInit {


  reports: Report[] | any

  constructor(
    private reportService: ReportService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.refreshReport()
  }

  refreshReport(){
    this.reportService.retriveSuccessReport().subscribe(
      (response: any)  => {
        console.log(response)
        this.reports = response
      }
    )
  }

}
