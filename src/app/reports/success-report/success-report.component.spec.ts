import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessReportComponent } from './success-report.component';

describe('SuccessReportComponent', () => {
  let component: SuccessReportComponent;
  let fixture: ComponentFixture<SuccessReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuccessReportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
